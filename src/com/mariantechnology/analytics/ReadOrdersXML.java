package com.mariantechnology.analytics;
 
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
 
/**
 * <strong>Process the Stamps.com order export XML file and extract the following data:</strong> <br />
 * Total customers, # of returning customers, email contact information, 
 * quantity of orders per customer, and a list of order IDs to back track on the server if necessary.
 * 
 * <p>Stamps.com export settings:<br />
 * Pending, Processing, Completed</p>
 * 
 * @author ARR
 *
 */
public class ReadOrdersXML {
	private static String path;
	private static final String FILENAME = "woocommerce-orders.xml";
	private static Date today;
	private static final String OUTPUT_FILE_PREFIX = "Report_";
	private static final String OUTPUT_FILE_EXTENSION = ".xls";
	private static final String DELIMETER = "\t";
	
	//data is appended to the String writeFile throughout the main method and written to a text file at the end 
	//these variables were taken out of main in order to out source some of the printing to make it simpler to read
	private static String writeFile = ""; 
	private static String newLine = "\n";
	private static final String DASHED_LINE = "-------------------------------------------------------------------";
 	
	public static void main(String argv[]) {
		String user = System.getProperty("user.home");
		path = user + "/Downloads/";
		
		//get today's date
		Calendar c = new GregorianCalendar();
	    today = c.getTime();
	    String fileName = OUTPUT_FILE_PREFIX + today + OUTPUT_FILE_EXTENSION;
	    
		try {
			printHeader();
		    LinkedList<Order> orderList = extractFromXMLandSortByEmail();
		    ArrayList<Customer> customers = importOrders(orderList);
		    int repeatCustomers = generateAndPrintReport(customers);
			printFooter(repeatCustomers, customers.size(), fileName);
			
			//output to text file
			FileUtils.writeStringToFile(new File(fileName), writeFile);
		} catch (Exception e) {
			System.out.println("Failed to load XML file");
			e.printStackTrace();
		}
		
	}
	
	/**
	 * This method prints to console, appends to the String writeFile, but does not alter the argument
	 * 
	 * @param customers
	 * @return how many customers have purchased more than once
	 */
	private static int generateAndPrintReport(ArrayList<Customer> customers) {
		int repeatCustomers = 0;
		for (Customer element : customers){
			System.out.println(element);
			writeFile = writeFile + element + newLine;
			if(element.orderCount>1){
				repeatCustomers++;
			}
		}
		return repeatCustomers;
	}

	/**
	 * This method requires that the orderList be sorted by email addresses. It compares the email addresses
	 * of two consecutive orders and if they are the same, it appends it to the first {@link Customer}, if not it will
	 * create a new {@link Customer}.
	 * 
	 * @param orderList
	 * @return
	 */
	private static ArrayList<Customer> importOrders(LinkedList<Order> orderList) {
		ArrayList<Customer> customers = new ArrayList<Customer>();
		
		//Set up import orders into customers. This is done to see how many customers have ordered more than once
		Customer customer = new Customer(); //first customer on the list
		customer.tryOrder(orderList.getFirst()); //insert the first order into the first customer
	    
		String email_1, email_2;
		email_1 = orderList.getFirst().toString(); //first email on orderList
		
		//import orders into customer array, it does so by comparing email addresses- requires sorted email list
		for (int i = 1; i<orderList.size(); i++){
			email_2 = orderList.get(i).toString();
			
			if(email_1.compareTo(email_2)==0){
				//same customer- add the order
				Order currentOrder = orderList.get(i);
				customer.tryOrder(currentOrder);
				if(i==orderList.size()-1){
					//save the customer to the ArrayList since this is the last iteration
					customers.add(customer);
				}
			} else {
				//this order belongs to a new customer so save the previous customer
				customers.add(customer);
				
				//blank it out... might not be necessary but it seems like it would make things clearer
				customer = null;
				
				//create a new customer and add the order
				customer = new Customer();
				Order currentOrder = orderList.get(i);
				customer.tryOrder(currentOrder);
			}
			
			//save the current email to test in the next iteration - it should always remain behind email_2
			email_1 = orderList.get(i).toString(); 
		}
		return customers;
	}

	
	/**
	 * Extracts the orders from the XML file and sorts them by email
	 * 
	 * @param doc
	 * @return a LinkedList of orders
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws SAXException 
	 */
	private static LinkedList<Order> extractFromXMLandSortByEmail() throws ParserConfigurationException, SAXException, IOException {
		File fXmlFile = new File(path + FILENAME);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);

		//optional, but recommended: http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
		doc.getDocumentElement().normalize();
		//extract from XML
		LinkedList<Order> orderList2 =  new LinkedList<Order>();
		NodeList nList = doc.getElementsByTagName("Item");
		for (int i = 0; i < nList.getLength(); i++) {
			Node nNode = nList.item(i);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				orderList2.add(new Order(
						eElement.getElementsByTagName("OrderDate").item(0).getTextContent(),
						eElement.getElementsByTagName("OrderID").item(0).getTextContent(),
						eElement.getElementsByTagName("FirstName").item(0).getTextContent(),
						eElement.getElementsByTagName("LastName").item(0).getTextContent(),
						eElement.getElementsByTagName("ZIP").item(0).getTextContent(),
						eElement.getElementsByTagName("Address").item(0).getTextContent()
						));
			} 
		}
		
		//sort orders by email (is required for importing Orders into Customers)
		Collections.sort(orderList2, new Comparator<Order>() {
		    public int compare(Order s1, Order s2) {
		        return s1.email.compareTo(s2.email);
		    }
		});
		return orderList2;
	}

	
	/**
	 * prints dashed line, total customers, and repeat customers to the console and appends it to the String writeFile
	 * 
	 * @param repeatCustomers
	 * @param customerSize
	 */
	private static void printFooter(int repeatCustomers, int customerSize, String filename) {
		System.out.println(DASHED_LINE);
		writeFile = writeFile + DASHED_LINE + newLine;
		
		String totalCustomers = "Total customers: " + customerSize;
		System.out.println(totalCustomers);
		writeFile = writeFile + totalCustomers + newLine;
		
		int decimalPoints = 1;
		float percentageRepeated = (float)repeatCustomers/customerSize*100;
		percentageRepeated = round(percentageRepeated, decimalPoints);
		String sRepeatCustomers = "Repeat customers: " + repeatCustomers + " (" + percentageRepeated + "%)";
		System.out.println(sRepeatCustomers);
		writeFile = writeFile + sRepeatCustomers + newLine;
		
		//for convenience
		System.out.println(filename);
	}
	
	/**
	 * prints the title, subtitle, and dashed line to console and appends it to the String writeFile
	 */
	private static void printHeader() {
			String columnTitles = 	"# of orders" 			+ DELIMETER +
									"days since last order"	+ DELIMETER +
									"first name"			+ DELIMETER +
									"last name"				+ DELIMETER +
									"email"					+ DELIMETER +
									"zip"					+ DELIMETER +
									"order IDs";
			System.out.println(columnTitles);
			writeFile = writeFile + columnTitles + newLine;
	}

	 /**
     * Round to certain number of decimals
     * 
     * @param d
     * @param decimalPlace
     * @return
     */
    public static float round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }
	
    /**
     * This class is designed to contain all of the relevant information about the customer. This includes multiple orders
     * 
     * @author arr
     *
     */
 	public static class Customer{
		public ArrayList<String> orderDate;
		public ArrayList<Integer> orderID;
		public String firstName;
		public String lastName;
		public String zip;
		public String email; //listed as "Address" in the orders XML file
		public int orderCount = 0;
		public Date latestOrderDate = null;
		
		public Customer(){
			orderDate = new ArrayList<String>();
			orderID = new ArrayList<Integer>();
		}
		
		/**
		 * Process the order and save it if the order number is unique
		 * 
		 * @param o
		 * @return
		 */
		public boolean tryOrder(Order o){
			boolean added = false;
			boolean duplicate = false;

			if(orderCount < 1){
				added = addOrder(o);
			} else {
				String testDate = o.date;
				//compare against the older
				for(int i = 0; i<orderCount; i++){
					if(testDate.compareTo(orderDate.toString())==0){
						duplicate = true;
						added = false;
						continue;
					}
				}
				
				if(duplicate == false){
					added = addAnotherOrder(o);
				}
			}

			return added;
		}
		
		/**
		 * @return A String containing all of the IDs separated by a period
		 */
		private String displayIDs(){
			String display = "";
			for(int ID : orderID){
				display = display + String.valueOf(ID) + ". ";
			}
			return display;
		}
		
		/**
		 * This method displays the Customer data. 
		 * @return orderCount | daysSinceOrder | firstName | lastName | email | order IDs
		 */
		public String toString() {
			String count = String.valueOf(orderCount);
			getLatestOrderDate(); //updates the variable in order to calculate daysBetween
			String daysSinceOrder = String.valueOf(daysBetween());
			String assembled = 
					count 		+ DELIMETER + daysSinceOrder 	+ DELIMETER +  
					firstName 	+ DELIMETER + lastName 			+ DELIMETER +  
					email 		+ DELIMETER + zip 				+ DELIMETER +  displayIDs();
		    return assembled;
		}
		
		/**
		 * Add the first order to Customer. This basically ports all of the Order data to Customer since everything is blank
		 * 
		 * @param o
		 * @return
		 */
		private boolean addOrder(Order o){
			orderDate.add(o.date);
			orderID.add(o.id);
			this.firstName = o.firstName;
			this.lastName = o.lastName;
			this.zip = o.zip;
			this.email = o.email;
			orderCount++;
			return true;
		}
	
		/**
		 * The Customer already exists in the system and a new order is appended to the class. This involves only extracting the
		 * Order date, id, and incrementing the order count.
		 * 
		 * @param o
		 * @return
		 */
		private boolean addAnotherOrder(Order o){
//			System.out.println("another order");
			orderDate.add(o.date);
			orderID.add(o.id);
			orderCount++;
			return true;
		}
 	
		/**
		 * Compares all of the order dates for the Customer and finds the most recent date
		 * @return the Date of the most recent order
		 */
		public Date getLatestOrderDate(){
			//get the dates stored in the ArrayList
			String[] sDates = orderDate.toArray(new String[orderDate.size()]);
			
			//It should match the XML date: 2013-04-14 14:08:35 
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			
			//parse all String dates into Date.class format
			Date []dates = new Date[sDates.length];
			for(int i = 0; i<sDates.length; i++){
				try {
					dates[i] = sdf.parse(sDates[i]);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			
			latestOrderDate = LatestDateFinder.findLatestDate(dates);
			return latestOrderDate;
		}
 	
		public int daysBetween(){
			Calendar cal1 = new GregorianCalendar();
		    Calendar cal2 = new GregorianCalendar();
		    cal2.setTime(today);
		    cal1.setTime(latestOrderDate);
		    return daysBetween(cal1.getTime(),cal2.getTime());
		}
		
		public static final int HOURS_IN_DAY = 24;
		public static final int MINUTES_IN_HOUR = 60;
		public static final int SECONDS_IN_MINUTE = 60;
		public static final int MILISECONDS_IN_SECONDS = 1000;
		
		public int daysBetween(Date d1, Date d2){
			int unitConversion = MILISECONDS_IN_SECONDS * SECONDS_IN_MINUTE * 
								MINUTES_IN_HOUR * HOURS_IN_DAY;//1000 * 60 * 60 * 24
            return (int)( (d2.getTime() - d1.getTime()) / unitConversion);
		}
 	
 	}
 	
 	/**
 	 * Holds the order data extracted from the XML file. 
 	 * <p>Contains: <br />
 	 * date, id, first name, last name, zip code, and email address</p>
 	 * @author arr
 	 *
 	 */
	public static class Order implements Comparator<Order>{
		public String date;
		public Integer id;
		public String firstName;
		public String lastName;
		public String zip;
		public String email; //listed as "Address" in the orders XML file
		
		public Order(String date, String id, String firstName, String lastName, String zip, String email){
			this.date = date;
			this.id = Integer.parseInt(id);
			this.firstName = firstName;
			this.lastName = lastName;
			this.zip = zip;
			this.email = email;
		}
		
		/**
		 * The orders are assigned to customers by comparing their email. TODO it might be a good idea to have a more
		 * robust way of assigning an order to a particular customer since they might use different email addresses for 
		 * different orders.
		 * 
		 * @return String email
		 */
		public String toString() {
			return email;
		}
		
		public void setDate(String date){
			this.date = date;
		}
		
		public void setID(String id){
			this.id = Integer.parseInt(id);
		}
		
		public void setFirstName(String firstName){
			this.firstName = firstName;
		}
		
		public void setLastName(String lastName){
			this.lastName = lastName;
		}
		
		public void setZip(String zip){
			this.zip = zip;
		}
		
		public void setEmail(String email){
			this.email = email;
		}

		@Override
		public int compare(Order o1, Order o2) {
			Order p1 = o1;
			Order p2 = o2;
			return p1.email.compareTo(p2.email);
		}
		
		
	}
	
}