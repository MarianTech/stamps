package com.mariantechnology.analytics;


import java.util.Date;


/**
 * This class demonstrates on how to find the latest date for a given set of dates. 
 * 
 * @author JavaIQ.net
 * Creation Date Dec 10, 2010
 */
public class LatestDateFinder {
    public LatestDateFinder() {
    }

    public static Date findLatestDate(Date date1, Date date2) {
        Date latestDate = null;

        if (date1 != null && date2 != null) {
            latestDate = date1.getTime() > date2.getTime() ? date1 : date2;
        }

        return latestDate;
    }

    public static Date findLatestDate(Date[] dates) {
        Date latestDate = null;

        if ((dates != null) && (dates.length > 0)) {
            for (Date date: dates) {
                if (date != null) {
                    if (latestDate == null) {
                        latestDate = date;
                    }
                    latestDate = date.after(latestDate) ? date : latestDate;
                }
            }
        }

        return latestDate;
    }

    /**
     * Method to test other methods in the class with sample inputs
     * @param args
     */
    public static void main(String[] args) {

    }
}
